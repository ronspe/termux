;; This is the configuration for Termux

;;{{{ variables
;; (setq gitdir (getenv "GITDIR")) 
;;}}}
;; -*- mode: Emacs-Lisp; fill-column: 75; comment-column: 50; -*-
;;{{{ Packages manual
;;;https://www.sobyte.net/post/2022-05/emacs-package/
(add-to-list 'load-path "~/.emacs.d/elpa/")
(let ((default-directory "~/.emacs.d/elpa"))
  (normal-top-level-add-subdirs-to-load-path))
;;}}}
;;{{{ Packages
(setq package-enable-at-startup t)
(require 'package)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package)
  (use-package-expand-minimally t)
  (require 'use-package))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("nongnu" . "https://elpa.nongnu.org/nongnu/"))
(add-to-list 'package-archives '("stable" . "https://stable.melpa.org/packages/"))
(setq package-archive-priorities '(("melpa"  . 100)
                                   ("gnu"    .  50)
                                   ("nongnu" .  25)))

;;}}} 
;;{{{ Some sensible settings
(setq gc-cons-threshold (* 50 1000 1000))               ; For faster startup. Default = 800kb
(setq load-prefer-newer noninteractive)                 ; Prefer loading newest compiled .el file
(desktop-save-mode 1)                                   ; reopen open buffers after restart
(savehist-mode)                                         ; Save history of minibuffer
(setq auto-revert-interval 3)                           ; check auto revert every 3 seconds
(setq auto-revert-check-vc-info t)                      ; auto revert updates version control data
(global-auto-revert-mode t)                             ; update buffers when a file is changed on disk by another process
(setq global-auto-revertnon-file-buffers t)             ; Revert Dired and other buffers
(setq-default major-mode 'fundamental-mode)             ; default mode for a new buffer
(setq backup-directory-alist `((".*" . ,temporary-file-directory)))          ; store all backup files in the tmp dir
;;(setq custom-file (concat user-emacs-directory "conf-min/custom-minimal.el"))                 ; store custom variables in another file
(global-subword-mode 1)                                 ; Easily navigate sillycased words
(setq-default truncate-lines t)                         ; Don't break lines

;;}}}
;;{{{ Dired settings
(setq dired-listing-switches "-agho --group-directories-first") ; Sort Dired buffers
(put 'dired-find-alternate-file 'disabled nil)                  ; Open dired folders in same buffer
(setq dired-dwim-target t)                                      ; Copy and move files between two dired buffers
(setq delete-by-moving-to-trash t)                              ; Move deleted files to trash

;;}}}
;;{{{ interface
(setq inhibit-startup-message t) ; no startup message
(menu-bar-mode 1)                ; Enable the menu bar
(scroll-bar-mode -1)             ; Disable visible scrollbar
(tool-bar-mode -1)               ; Disable the toolbar
(tooltip-mode -1)                ; Disable tooltips
;;(set-fringe-mode 10)             ; Give some breathing room in the fringe
(show-paren-mode 1)              ; turn on paren match highlighting
(setq visible-bell t)            ; Set up the visible bell
(fset 'yes-or-no-p 'y-or-n-p)    ; “yes or no” prompt and replace it with “y or n”:
(setq x-underline-at-descent-line nil)            ; Prettier underlines
(setq switch-to-buffer-obey-display-actions t)    ; Make switching buffers more consistent
;; Linenumbers ---------------------------------------------------
(column-number-mode) ;; Linenumbers
(display-line-numbers-mode 1)
;; relative line numbering
(setq-default display-line-numbers 'visual
    display-line-numbers-widen t
    display-line-numbers-current-absolute t)
;; Language and UTF-8 settings ---------------------------------------
(set-language-environment "English")
(set-locale-environment "en_US.UTF-8")
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
;; Treat clipboard input as UTF-8 string first; compound text next, etc.
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))
;;}}}
;;{{{ Whitespace, parenthesis, align text
(setq-default indent-tabs-mode nil)                 ; Use spaces instead of tabs
;;}}}
;;{{{ minibuffer completion
;; For help, see: https://www.masteringemacs.org/article/understanding-minibuffer-completion
(setq enable-recursive-minibuffers t)                             ; Use the minibuffer whilst in the minibuffer
(setq completion-cycle-threshold 1)                               ; TAB cycles candidates
(setq completions-detailed t)                                     ; Show annotations
(setq tab-always-indent 'complete)                                ; When I hit TAB, try to complete, otherwise, indent
(setq completion-styles '(flex basic partial-completion emacs22)); Different styles to match input to candidates
;;}}}
;;{{{ Compeletion framework based on Vertico, Marginalia, Consult, Orderless
;;; Vertico
 (use-package vertico
  :bind (:map vertico-map
              ("C-f" . vertico-insert)
              ("C-n" . vertico-next)
              ("C-t" . vertico-previous)
              ("M-t" . vertico-directory-up)
              )
  :custom (vertico-cycle t)
  :init (vertico-mode 1)
  :config
  (require 'vertico-directory)
  )

;; Completion with Vertico
(bind-key "C-f" #'completion-at-point)

;;; Marginalia
(use-package marginalia
  :custom (marginalia-annotators (marginalia-annotators-heavy marginalia-annotators-light nil))
  :init
  (marginalia-mode 1))

(defun ron/completion-with-vertico (&rest args)
    "Use `consult-completion-in-region' if Vertico is enabled."
    (if vertico-mode
        (apply #'consult-completion-in-region args)
      (apply #'completion--in-region args)
      ))
(setq completion-in-region-function #'ron/completion-with-vertico)
(bind-key [remap isearch-edit-string] #'consult-isearch-history isearch-mode-map)

;;; Orderless
(use-package orderless
  :ensure t
  :config
  (setq completion-styles '(orderless partial-completion basic)))

;;}}}
;;{{{ global key bindings
(global-set-key (kbd "M-i") 'imenu)
(global-set-key (kbd "C-x C-b") 'ido-switch-buffer)
(global-set-key (kbd "C-x b") 'ibuffer)
;;}}}
;;{{{ Recentf
(recentf-mode 1)
(setq recentf-max-menu-items 50)
(setq recentf-max-saved-items 500)
(setq recentf-exclude '(".*/elpa.+"
                        ".*agenda.*.org"))
;;}}}
;;{{{ Golden ratio
(add-to-list 'load-path "~/.emacs.d/manual-packages/golden-ratio")
(require ' golden-ratio)
(golden-ratio-mode 1)
;;}}}
;;{{{ Denote
;; https://protesilaos.com/emacs/denote
;; Make Elisp files in that directory available to the user.
(add-to-list 'load-path "~/.emacs.d/manual-packages/denote")
(require ' denote)
;;(setq denote-directory (expand-file-name "denotes" org-directory))
(setq denote-date-prompt-use-org-read-date t)  ;; pick dates
(add-hook 'dired-mode-hook #'denote-dired-mode)
(add-hook 'context-menu-functions #'denote-context-menu)
(let ((map global-map))
  (define-key map (kbd "C-c n j") #'my-denote-journal) ; our custom command
  (define-key map (kbd "C-c n n") #'denote)
  (define-key map (kbd "C-c n N") #'denote-type)
  (define-key map (kbd "C-c n d") #'denote-date)
  (define-key map (kbd "C-c n s") #'denote-subdirectory)
  (define-key map (kbd "C-c n t") #'denote-template)
  ;; If you intend to use Denote with a variety of file types, it is
  ;; easier to bind the link-related commands to the `global-map', as
  ;; shown here.  Otherwise follow the same pattern for `org-mode-map',
  ;; `markdown-mode-map', and/or `text-mode-map'.
  (define-key map (kbd "C-c n i") #'denote-link) ; "insert" mnemonic
  (define-key map (kbd "C-c n I") #'denote-link-add-links)
  (define-key map (kbd "C-c n b") #'denote-link-backlinks)
  (define-key map (kbd "C-c n F") #'denote-link-find-file)
  (define-key map (kbd "C-c n B") #'denote-link-find-backlink)
  ;; Note that `denote-rename-file' can work from any context, not just
  ;; Dired bufffers.  That is why we bind it here to the `global-map'.
  (define-key map (kbd "C-c n r") #'denote-rename-file)
  (define-key map (kbd "C-c n R") #'denote-rename-file-using-front-matter))

;; Key bindings specifically for Dired.
(let ((map dired-mode-map))
  (define-key map (kbd "C-c C-d C-i") #'denote-link-dired-marked-notes)
  (define-key map (kbd "C-c C-d C-r") #'denote-dired-rename-marked-files)
  (define-key map (kbd "C-c C-d C-R") #'denote-dired-rename-marked-files-using-front-matter))

(defun ron-denote-from-region (beg end)
   "Create note whose contents include the text between BEG and END.
   Prompt for title and keywords of the new note."
  (interactive "r")
  (if-let (((region-active-p))
           (text (buffer-substring-no-properties beg end)))
      (progn
        (denote (denote--title-prompt) (denote--keywords-prompt))
        (insert text))
    (user-error "No region is available")))
;;}}}
;;{{{ Which key
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(add-to-list 'load-path "~/.emacs.d/manual-packages/which-key")
(require 'which-key)
(which-key-mode)
;;}}}
;;{{{ Corfu completion UI
;;https://github.com/minad/corfu
(use-package corfu
  ;; Optional customizations
  :custom
  (corfu-cycle t)                ;; Enable cycling for `corfu-next/previous'
  (corfu-auto t)                 ;; Enable auto completion
  (setq corfu-auto-delay 0.2)    ;; Set the delay before automatic completion
  (corfu-separator ?\s)          ;; Orderless field separator
  (corfu-quit-at-boundary t)     ;; quit at completion boundary
  ;; (corfu-quit-no-match nil)      ;; Never quit, even if there is no match
  ;; (corfu-preview-current nil)    ;; Disable current candidate preview
  (corfu-preselect 'prompt)      ;; Preselect the prompt
  ;; (corfu-on-exact-match nil)     ;; Configure handling of exact matches
  ;; (corfu-scroll-margin 5)        ;; Use scroll margin
  (corfu-popupinfo-delay '(0.25 . 0.1))
  (corfu-popupinfo-hide nil)

  ;; Use TAB for cycling, default is `corfu-complete'.
  :bind
    (:map corfu-map
        ("TAB" . corfu-next)
        ([tab] . corfu-next)
        ("S-TAB" . corfu-previous)
        ([backtab] . corfu-previous))
:config
  (corfu-popupinfo-mode)
:init
  (global-corfu-mode))

;; A few more useful configurations...
(use-package emacs
  :init
  ;; TAB cycle if there are only few candidates
  (setq completion-cycle-threshold 3)
  ;; Emacs 28: Hide commands in M-x which do not apply to the current mode.
  ;; Corfu commands are hidden, since they are not supposed to be used via M-x.
  ;; (setq read-extended-command-predicate
  ;;       #'command-completion-default-include-p)
  )

(use-package cape
;; Bind dedicated completion commands
:bind (("C-c c p" . completion-at-point) ;; capf
;;          ("C-c c t" . complete-tag)        ;; etags
       ("C-c c d" . cape-dabbrev)        ;; or dabbrev-completion
;;          ("C-c c h" . cape-history)
       ("C-c c f" . cape-file)
;;          ("C-c c k" . cape-keyword)
;;          ("C-c c s" . cape-symbol)
;;          ("C-c c a" . cape-abbrev)
;;          ("C-c c i" . cape-ispell)
;;          ("C-c c l" . cape-line)
;;          ("C-c c w" . cape-dict)
;;         ;; ("C-c c \\" . cape-tex)
;;         ;; ("C-c c _" . cape-tex)
;;         ;; ("C-c c ^" . cape-tex)
;;         ;; ("C-c c &" . cape-sgml)
;;         ;; ("C-c c r" . cape-rfc1345)
)
:init
;;(global-corfu-mode)
;; Add `completion-at-point-functions', used by `completion-at-point'.
   (add-to-list 'completion-at-point-functions #'cape-dabbrev)
   (add-to-list 'completion-at-point-functions #'cape-file)
;;   (add-to-list 'completion-at-point-functions #'cape-history)
;;   (add-to-list 'completion-at-point-functions #'cape-keyword)
;;   ;;(add-to-list 'completion-at-point-functions #'cape-tex)
;;   ;;(add-to-list 'completion-at-point-functions #'cape-sgml)
;;   ;;(add-to-list 'completion-at-point-functions #'cape-rfc1345)
;;   (add-to-list 'completion-at-point-functions #'cape-abbrev)
;;   (add-to-list 'completion-at-point-functions #'cape-ispell)
;;   (add-to-list 'completion-at-point-functions #'cape-dict)
;;   (add-to-list 'completion-at-point-functions #'cape-symbol)
;;   (add-to-list 'completion-at-point-functions #'cape-line)
)
;;}}}
;;{{{ folding
;;https://www.emacswiki.org/emacs/FoldingMode#h5o-4
(add-to-list 'load-path "~/.emacs.d/manual-packages/folding")
(if (require 'folding nil 'noerror)
        (folding-mode-add-find-file-hook)
  (message "Library `folding' not found"))
(autoload 'folding-mode "folding" "Folding mode" t)
(setq folding-narrow-by-default nil)  ;; do not narrow
(folding-add-to-marks-list 'sql-mode "--{{{" "--}}}" nil t)
(define-key global-map (kbd "C-c z") #'folding-toggle-show-hide)
(folding-mode)
;;}}}
;;{{{ expand-region
(use-package expand-region
  :bind ("C-=" . er/expand-region)
        ("C--" . er/contract-region))
;;}}}
;;{{{ whole-line-or-region
;; https://github.com/purcell/whole-line-or-region
(add-to-list 'load-path "~/.emacs.d/manual-packages/whole-line-or-region")
(require 'whole-line-or-region) 
(whole-line-or-region-global-mode)
 ;;}}}
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(magit auctex vertico marginalia corfu use-package orderless expand-region)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
