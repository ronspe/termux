#!/usr/bin/env bash
# USE:
# curl -LO https://gitlab.com/ronspe/termux/-/raw/main/termux-setup.sh
# bash termux-setup.sh

color_scheme_url="https://gitlab.com/ronspe/termux/-/raw/main/colors/colors.properties"
font_url="https://gitlab.com/ronspe/termux/-/raw/main/fonts/iosevka-comfy-regular.ttf"
emacs_url="https://gitlab.com/ronspe/termux/-/raw/main/.emacs.d"
termux_dir="$HOME/.termux"

# Updating and upgrading packages
pkg update
pkg upgrade -y

# Cleaning up packages
pkg autoclean
pkg clean

# Setup termux storage
termux-setup-storage

# Install apps
pkg install git emacs curl wget zip grep rsync openssh python python-pip termux-exec termux-api which zsh -y

# Change default shell to zsh
chsh -s zsh

mkdir -p $HOME/.config
#curl -sSL -o $HOME/.config/starship.toml "$starship_config_url"

echo "Get Termux from gitlab"
mkdir -p $HOME/gitlab
cd gitlab
git clone https://gitlab.com/ronspe/termux

echo "Make softlinks"
ln -s ~/gitlab/termux/.config ~/.config/ 
ln -s ~/gitlab/termux/.emacs.d/ ~/.emacs.d 
ln -s ~/gitlab/termux/.zshrc ~/.zshrc 
ln -s ~/gitlab/termux/.aliases ~/.aliases
ln -s ~/gitlab/termux/bin ~/bin

# Create a symbolic link for easy access to nvim and yt-dlp
#ln -s $(which nvim) $PREFIX/bin/vim
#ln -s $(which yt-dlp) $PREFIX/bin/youtube-dl

# Set some aliases
# echo "alias l=\"ls -la\"" >> ~/.zshrc
#echo "alias yt-dlp=\"yt-dlp -o '%(title)s.%(ext)s' --external-downloader aria2c --external-downloader-args '-c -j 4 -x 8 -s 8 -k 1M'\"" >> ~/.zshrc

# Setup color scheme
echo "Installing color schemes..."
curl -sSL -o  "$termux_dir/colors.properties" "$color_scheme_url"

# Install font
echo "Installing Fonts..."
curl -sSL -o "$termux_dir/font.ttf" "$font_url"

# Done
echo '\nDone!'
