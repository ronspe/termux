# This is for Termux
# History {{{2
export HISTSIZE=100000
export HISTFILE="$HOME/.history"
export SAVEHIST=$HISTSIZE
export HISTCONTROL=ignoredups:erasedups:ignorespaces
export HISTIGNORE="pwd:l:ls:ls -ltr:cd:c:cdaudi:cdpy:cddesk:cdsql:"
# options {{{2
setopt autocd               # .. is shortcut for cd .. (etc)
setopt autoparamslash       # tab completing directory appends a slash
setopt autopushd            # cd automatically pushes old dir onto dir stack
setopt clobber              # allow clobbering with >, no need to use >!
ssetopt correct              # command auto-correction
setopt correctall           # argument auto-correction
setopt noflowcontrol        # disable start (C-s) and stop (C-q) characters
setopt nonomatch            # unmatched patterns are left unchanged
setopt histignorealldups    # filter duplicates from history
setopt histignorespace      # don't record commands starting with a space
setopt histverify           # confirm history expansion (!$, !!, !foo)
setopt ignoreeof            # prevent accidental C-d from exiting shell
setopt interactivecomments  # allow comments, even in interactive shells
setopt printexitvalue       # for non-zero exit status
setopt pushdignoredups      # don't push multiple copies of same dir onto stack
setopt pushdsilent          # don't print dir stack after pushing/popping
setopt sharehistory         # share history across shells

autoload zmv  # bulk rename files


# enable menu-style completion for cdr
zstyle ':completion:*:*:cdr:*:*' menu selection

# fall through to cd if cdr is passed a non-recent dir as an argument
zstyle ':chpwd:*' recent-dirs-default true

# User settings {{{1
# export variables {{{2
#export CURRENTSHELL=`ps -hp $$|awk '{print $5}'`
export GITDIR=$HOME/gitlab
export LANG=en_US.UTF-8
export LC_ALL=$LANG
export LANGUAGE=$LANG
export LC_CTYPE=$LANG
export LC_MESSAGES=$LANG

#PATH
PATH=$PATH
PATH=$PATH:$GITDIR/termux
PATH=$PATH:$GITDIR/bin
export PATH
CDPATH=.:..
CDPATH=$CDPATH:~
CDPATH=$CDPATH:$GITDIR
CDPATH=$CDPATH:$GITDIR/termux
export CDPATH

###### APPS
export PAGER="less -S"
export ALTERNATE_EDITOR='emacsclient -nw --alternate-editor=""'
export EDITOR='emacsclient -nw --alternate-editor=""'
# for ctag
export LC_COLLATE=C

# starts one or multiple args as programs in background
start_in_background() {
  for ((i=2;i<=$#;i++)); do
    ${@[1]} ${@[$i]} &> /dev/null &
  done
}

PROMPT="%~>"
#####################################################################
# SHORTCUTS
# clears the shell and displays the current dir
clear-ls-all() {
    clear
    ls -AF
}
zle -N clear-ls-all
bindkey '^N' clear-ls-all

# clears the shell and displays the dir tree with level 2
clear-tree-2() {
    clear
    tree -a -F -L 2
}
zle -N clear-tree-2
bindkey '^K' clear-tree-2

# clears the shell and displays the dir tree with level 3
clear-tree-3() {
    clear
    tree -a -F -L 3
}
zle -N clear-tree-3
bindkey '^H' clear-tree-3

# add sudo before command with esc, esc
function prepend-sudo() {
  [[ -z $BUFFER ]] && zle up-history
  if [[ $BUFFER == sudo\ * ]]; then
    LBUFFER="${LBUFFER#sudo }"
  else
    LBUFFER="sudo $LBUFFER"
  fi
}
zle -N prepend-sudo
# shortcut keys: [Esc] [Esc]
bindkey "\e\e" prepend-sudo

# load aliases
if [ -f $GITDIR/termux/.aliases ]; then
    source $GITDIR/termux/.aliases
else
    print "404: $GITDIR/termux/.aliases not found."
fi
